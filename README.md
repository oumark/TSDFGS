# TSDFGS

## Training Set Determination for Genomic Selection

<center><a href="mailto:oumark.me@outlook.com"><strong>Jen-Hsiang Ou</strong></a> and <a href="mailto:ctliao@ntu.edu.tw"><strong>Chen-Tuo Liao</strong></a></center>

---

Determining an optimal training set is an important issue for genomic selection. We consolidate two awesome criteria, PEV (Akdemir et al., 2015) and CD (Laloe, 1993) and a new criterion, r-score. All three criterions can be used in both highly structured and mild population.

R-package [DOWNLOAD](./TSDFGS_1.0.tar.gz)

- **Type** Package
- **Version** 1.0
- **Date** 2019.01.16
- **Authors** Jen-Hsiang Ou, Chen-Tuo Liao
- **Maintainer** Jen-Hsiang Ou (oumark.me@outlook.me)
- **License** GPL-3
- **Depends** R(>=2/10), Rcpp, RcppEigen

### Functions

```{R}
optTrain(geno, cand, n.train, subpop=NULL, test=NULL, method="rScore", min.iter=NULL)
```

**Arguments**

- `geno`

  A numeric principle component matrix (row: individuals, column: PCs). To reduce computing time, we may use first `k` PCs by `geno[,1:k]`.

- `cand`

  A numeric vector of candidate sets' row number in `geno` matrix.

- `n.train`

  The number of individuals to select in the training set.

- `subpop`

  A character vector of the names of subpopulation. Remain `NULL` if the population structure is mild. One may also use the method which considered population structure by giving a vector with all the same sub-pop name.

- `test`

  A numeric vector of test sets' row number in `geno` matrix. If it remains `NULL`, un-target methods will be used.

- `method`

  Choices are `rScore`, `PEVfull` and `CD`. `rScore` will be used by default.

- `min.iter`

  Minimum iteration for all algorithm can be appoint. One should always check if the algorithm is converges or not. A minimum iteration number will be set if it remains `NULL`.



**Note**

Both genetic algorithm simple exchange algorithms do not convergence to global optimal and it is highly recommended to draw the convergence plot to make sure it converges to the local optimal.



**Return**

- `DETtrain`

  An integer vector of chosen optimal training set.

- `ITERscore`

  Score of each iteration. (Given by one of three criteria)

- `TOPscore`

  Score of the best solution in by far. (Given by one of three criteria)



**Installation**

```{R}
devtools::install_gitlab("oumark/TSDFGS")
```

Windows users should have [Rtools](https://cloud.r-project.org/bin/windows/Rtools/) to build the function.





---

<center>
	Maintained by: <a href="https://www.oumark.me">Ou, Mark!</a><br />
	<a href="mailto:oumark.me@outlook.com">oumark.me@outlook.com</a>
</center>

